#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/zlib/Regression/bz1409740-zlib-does-not-pass-through-hardening-flags-during
#   Description: Test for BZ#1409740 (zlib does not pass through hardening flags during)
#   Author: Vaclav Danek <vdanek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="zlib"

rlJournalStart
    rlPhaseStartSetup
        rlRun "rlImport distribution/epel" || rlDie
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlRun "yum -y --enablerepo epel install checksec"
    rlPhaseEnd

    rlPhaseStartTest
        if [ "$(rlGetArch)" == "ppc64" ]; then
            [ -f /usr/lib64/libz.so.1.2.7 ] && rlRun -s "checksec -f /usr/lib64/libz.so.1.2.7"
            [ -f /usr/lib64/libz.so.1.2.7 ] && rlAssertGrep "Full RELRO" $rlRun_LOG
            [ -f /usr/lib64/libz.so.1.2.7 ] && rm -f $rlRun_LOG
            [ -f /usr/lib/libz.so.1.2.7 ] && rlRun -s "checksec -f /usr/lib/libz.so.1.2.7"
            [ -f /usr/lib/libz.so.1.2.7 ] && rlAssertGrep "Full RELRO" $rlRun_LOG
            [ -f /usr/lib/libz.so.1.2.7 ] && rm -f $rlRun_LOG
        else
            [ -f /usr/lib64/libz.so.1.2.7 ] && rlRun -s "checksec --file=/usr/lib64/libz.so.1.2.7"
            [ -f /usr/lib64/libz.so.1.2.7 ] && rlAssertGrep "Full RELRO" $rlRun_LOG
            [ -f /usr/lib64/libz.so.1.2.7 ] && rm -f $rlRun_LOG
            [ -f /usr/lib/libz.so.1.2.7 ] && rlRun -s "checksec --file=/usr/lib/libz.so.1.2.7"
            [ -f /usr/lib/libz.so.1.2.7 ] && rlAssertGrep "Full RELRO" $rlRun_LOG
            [ -f /usr/lib/libz.so.1.2.7 ] && rm -f $rlRun_LOG
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
