#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/zlib/Regression/bz754694-lib64-libz-so-1-no-version-information-available
#   Description: Test for BZ#754694 (/lib64/libz.so.1 no version information available)
#   Author: Tomas Dolezal <todoleza@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="zlib"
TEST_SANITY=0

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
#       rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
#       rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        for lib in $(rpm -ql zlib | grep ".so"); do
            if [ -h $lib ] ; then
                let TEST_SANITY++
                rlRun "eu-readelf -s $lib | grep -q '@@ZLIB_'" 0 "$lib contains version info"
           fi
        done

        if [ $TEST_SANITY -eq 0 ]; then
            rlFail "test fail - broken"
        fi
    rlPhaseEnd

#   rlPhaseStartCleanup
#       rlRun "popd"
#       rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
#   rlPhaseEnd
rlJournalPrintText
rlJournalEnd
