#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/zlib/Regression/bz690835-Please-enable-minizip-in-zlib-spec-file
#   Description: Test for BZ#690835 (Please enable minizip in zlib spec file)
#   Author: Petr Sklenar <psklenar@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh

PACKAGE="zlib"
MINIZIP_PACKAGE="minizip-compat-devel"
if rlIsRHEL '<9'; then
    MINIZIP_PACKAGE="minizip-devel"
fi

rlJournalStart
    rlPhaseStartSetup
    	# Since RHEL-9 the minizip-devel was replaced by minizip-compat-devel
	# so we need to remove it before installing the compat package
	# they conflict with each other
    	if ! rlIsRHEL '<9' ; then
	    rlRun "yum remove minizip-devel -y" 0,1
	    rlRun "rpm -q $MINIZIP_PACKAGE || yum install $MINIZIP_PACKAGE -y"
	fi
	rlAssertRpm $MINIZIP_PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
	    rlRun "tar -C $TmpDir -xzf minizip.tar.gz"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
	    rlRun "make run"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
