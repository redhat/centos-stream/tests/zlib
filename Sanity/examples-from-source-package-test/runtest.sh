#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/zlib/Sanity/exampeles-from-source-package-test
#   Description: it compile example app in source package and run it and try to find if no regression
#   Author: Jan Scotka <jscotka@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="zlib"
TMPDIR=`mktemp -d`
rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        cp example.c example-old.c gun.c $TMPDIR/
        pushd $TMPDIR
	if rlTestVersion "$(rpm -q zlib --qf '%{version}\n' | head -n 1)" ">" "1.2.11"; then
	        rlRun "cc -o example example.c -lz"
	else
	        rlRun "cc -o example example-old.c -lz"
	fi
        rlAssertExists "example"
        rlRun "cc -o gun gun.c -lz"
        rlAssertExists "gun"
        rlRun "cat gun.c | gzip > gunc.gz"
    rlPhaseEnd

    rlPhaseStartTest
        rlLog "example tests"
        rlRun "./example |grep 'uncompress(): hello, hello!'"
        rlRun "./example |grep 'gzread(): hello, hello!'"
        rlRun "./example |grep 'gzgets() after gzseek:  hello!'"
        rlRun "./example |grep 'inflate(): hello, hello!'"
        rlRun "./example |grep 'large_inflate(): OK'"
        rlRun "./example |grep 'after inflateSync(): hello, hello!'"
        rlRun "./example |grep 'inflate with dictionary: hello, hello!'"
        rlRun "zcat foo.gz|grep 'hello, hello!'"

        rlLog "gunzipping abilities of libz"
        rlRun "cat gunc.gz |./gun >gun.cx"
        rlRun "diff gun.c gun.cx" 0 "must be same, without any diff, it means decompress PASSed"
    rlPhaseEnd

    rlPhaseStartCleanup
        popd
        rlRun "rm -rf $TMPDIR"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
